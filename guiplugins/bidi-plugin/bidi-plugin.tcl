# META bidi plugin
# META DESCRIPTION bidirectional text input
# META AUTHOR IOhannes m zm�lnig <zmoelnig@umlaeute.mur.at>
# META VERSION 0.1

package require Tcl 8.4

set auto_path_bidi $auto_path
lappend auto_path $::current_plugin_loadpath

if {[catch {package require fribidi} result]} {
pdtk_post "failed to load bidi-plugin: $result\n"
} else {

###########################################################
# overwritten procedures
rename pdtk_text_set pdtk_text_set_bidi
rename pdtk_text_new pdtk_text_new_bidi

###########################################################
# overwritten

# change the text in an existing text box
proc pdtk_text_set {tkcanvas tag text} {
    pdtk_text_set_bidi $tkcanvas $tag [fribidi::log2vis $text]
}
proc pdtk_text_new {tkcanvas tags x y text font_size color} {
    pdtk_text_new_bidi $tkcanvas $tags $x $y [fribidi::log2vis $text] $font_size $color
}
pdtk_post "loaded: bidi-plugin 0.1\n"
}
set auto_path $auto_path_bidi
