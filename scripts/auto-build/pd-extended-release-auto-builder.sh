#!/bin/sh
# this script is the first attempt to have an automated updater and builder

# the source dir where this script is
## this could be done more easily with ${0%/*}
SCRIPT_DIR=$(echo $0 | sed 's|\(.*\)/.*$|\1|')
. $SCRIPT_DIR/auto-build-common

# the name of this script
## this could be done more easily with ${0##*/}
SCRIPT=$(echo $0| sed 's|.*/\(.*\)|\1|g')

BUILD_DIR=.
case $SYSTEM in 
    linux)
        BUILD_DIR=linux_make
        echo "Configuring to use $BUILD_DIR on GNU/Linux"
        ;;
    darwin)
        BUILD_DIR=darwin_app
        echo "Configuring to use $BUILD_DIR on Darwin/Mac OS X"
        ;;
    mingw*)
        BUILD_DIR=win32_inno
        echo "Configuring to use $BUILD_DIR on MinGW/Windows"
        ;;
    cygwin*)
        BUILD_DIR=win32_inno
        echo "Configuring to use $BUILD_DIR on Cygwin/Windows"
        ;;
    *)
        echo "ERROR: Platform $SYSTEM not supported!"
        exit
        ;;
esac


# convert into absolute path
cd "${SCRIPT_DIR}/../.."
auto_build_root_dir=`pwd`
echo "build root: $auto_build_root_dir" 

cd "${auto_build_root_dir}/packages/$BUILD_DIR"
make -C "${auto_build_root_dir}/packages" set_version
make test_locations
echo "mounts ----------------------------------------"
mount
print_ip_address
make package_clean
make install && make package
